var QModule = function($, window, document) {

	return function() {

		var obj = {};

		obj.options = {};

		// Show the questionaire in given div container
		obj.Show = function(param) {

			obj.options = param;

			var container = param.container;
			var questions = param.questions;
			var title = param.title;
			var service = {};

			service.createItem = function() {

				//Create title of questionaire
				service.createTitle().appendTo(container);;

				questions.forEach(function(element) {

					if (element.type == "checkbox") {
						service.createCheckBox(element).appendTo(container);
					} else if (element.type == "text") {
						service.createTextBox(element).appendTo(container);
					} else if (element.type == "radio") {
						service.createRadioBox(element).appendTo(container);
					} else if (element.type == "select") {
						service.createSelectBox(element).appendTo(container);
					} else if (element.type == "textarea") {
						service.createTextArea(element).appendTo(container);
					}

				});

				//Create required information
				service.createRequiredInfo().appendTo(container);;
			};

			service.createTitle = function() {
				var iDiv = $('<div />', {
					id: "QTitle",
					align: "center"
				});

				var label = $('<label />', {
					text: title
				});
				label.appendTo(iDiv);
				return iDiv;
			};

			service.createRequiredInfo = function() {
				var iDiv = $('<div />', {
					id: "QTitle",
					align: "right"
				}).html("<i>*required</i>");
				return iDiv;
			};

			service.createCheckBox = function(element) {
				var iDiv = service.createParentElement(element);
				//loop given options  
				element.options.forEach(function(option) {
					$('<label />').html(option.opt).prepend(
						$('<input />', {
							type: 'checkbox',
							id: "ID" + element.id + "_" + option.opt,
							name: element.id
						})).appendTo(iDiv);
					$('<br />').appendTo(iDiv);
				});

				return iDiv;
			};

			service.createRadioBox = function(element) {
				var iDiv = service.createParentElement(element);
				//loop given options  
				element.options.forEach(function(option) {
					$('<label />').html(option.opt).prepend(
						$('<input />', {
							type: 'radio',
							id: "ID" + element.id + "_" + option.opt,
							name: element.id,
							text: option.opt
						})).appendTo(iDiv);
					$('<br />').appendTo(iDiv);
				});

				return iDiv;
			};

			service.createSelectBox = function(element) {
				var iDiv = service.createParentElement(element);
				//loop given options  
				var select = $('<select />', {
					id: "ID" + element.id,
					name: element.id
				});
				element.options.forEach(function(option) {
					$("<option />", {
						value: option.opt,
						text: option.opt
					}).appendTo(select);
				});

				select.appendTo(iDiv);
				$('<br />').appendTo(iDiv);

				return iDiv;
			};

			service.createTextBox = function(element) {
				//div parent
				var iDiv = service.createParentElement(element);
				//create textbox
				$('<input />', {
					type: 'text',
					id: "ID" + element.id,
					//width: "40%"
				}).appendTo(iDiv);
				$('<br />').appendTo(iDiv);

				return iDiv;
			};

			service.createTextArea = function(element) {
				//div parent
				var iDiv = service.createParentElement(element);
				//create textbox
				$('<textarea  />', {
					id: "ID" + element.id,
					//width: "100%"
				}).appendTo(iDiv);
				$('<br />').appendTo(iDiv);

				return iDiv;
			};

			service.createParentElement = function(element) {
				//div parent
				var iDiv = $('<div />', {
					id: element.id
				});

				//label for question
				var label = $('<label />', {
					text: element.question
				});
				label.appendTo(iDiv);

				//If required
				if (element.required == true) {
					var label = $('<label />', {
						text: "*"
					});
					label.appendTo(iDiv);
				}

				$('<br />').appendTo(iDiv);

				return iDiv;
			};

			service.createItem();

		}

		obj.GetAnswer = function() {
			//Get all aswers from the questions  
			var answers = [];
			obj.options.questions.forEach(function(q) {
				
				var qAnswer = {};
				qAnswer.id = q.id;
				
				if (q.type == "text" || q.type == "select" || q.type == "textarea") { 
					qAnswer.value = $('#' + "ID" + q.id).val();
					answers.push(qAnswer);  
				} 
				else if (q.type == "checkbox") { 
					qAnswer.option = [];
					q.options.forEach(function(o) {
						var ans = {};
						ans.opt = o.opt
						ans.value = $('#' + "ID" + q.id + "_" + o.opt).is(':checked');
						qAnswer.option.push(ans);
					})

					answers.push(qAnswer);

				}  
				else if (q.type == "radio") { 
					qAnswer.value = $("input[name=" + q.id +"]:checked").text();
					answers.push(qAnswer);   
				}  

			}); 
			return answers;
		}

		// Perform validation
		obj.Validate = function() {

			var deferred = Q.defer(); 
			var result = {}; 

			if (!obj.options) {

				//if the questionaire is not correctly instantiated, reject the promise by saying message error
				result.message = "Questionaire options is not correctly instantiated"; 
				result.status = "error";
				deferred.reject(result);

			} 

			else {
				//Get the answer of the questionaire
				var answers = obj.GetAnswer();

				/*
					To do: Asynchronously perform validation send the questionaire answers to server
					Write the code here
				*/

				//Let say the answer from server is "Success", the resolve the deferred promise message as success
				result.message = "Success Validation!";
				result.status = "ok";
				deferred.resolve(result);
			}

			return deferred.promise;
		}

		return obj;
	}

}($, window, document);